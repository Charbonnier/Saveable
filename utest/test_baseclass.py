from unittest import TestCase

from potable import Saveable, State, Serializer, Deserializer
import json


class SampleSaveable(Saveable):
    def __init__(self, **kwargs) -> None:
        super().__init__()
        self.attributes = kwargs

    def __eq__(self, other):
        return self.attributes == other.attributes

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters(self.attributes)
        return state

    def set_state(self, state: State):
        self.attributes.update(state.get_parameters())


SampleSaveable.register_class()


class TestSerializer(TestCase):
    def test_serialize(self):
        saveable = SampleSaveable(string='Hello', integer=42, float=3.14, dictionary={1: 'one', 2: 'two'},
                                  list=[SampleSaveable(string='test'), 1, [2, 3]])
        serialized = Serializer().serialize(saveable)
        print(json.dumps(serialized, indent=2))
        new_saveable = Deserializer().deserialize(serialized)
        self.assertEqual(saveable, new_saveable)
