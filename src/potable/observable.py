import logging
from abc import abstractmethod
from typing import Callable, Set, Dict, Iterable

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)


class BaseObservableImplementation:
    @abstractmethod
    def add_event(self, event: str):
        """ Adds a new supported event

        :param event: The event to register to the class.
        """
        pass

    @abstractmethod
    def add_event_observer(self, event: str, observer: Callable) -> int:
        """ Adds an observer to an event.

        :param event: The identifier of the event to register to.
        :param observer: The method to call when the event is emitted.
        :return:
        """
        pass

    @abstractmethod
    def remove_event_observer(self, observer_id: int):
        """ Removes an observer from the class.

        :param observer_id: The id of the observer to remove.
        """
        pass

    @abstractmethod
    def remove_all_observers(self):
        """ Removes all registered observer. This function is called when the Observable is no longer used.

        """
        pass

    @abstractmethod
    def get_event_observers(self, event: str) -> Iterable[Callable]:
        """ Returns all the observer registered to the event given at input. This method is called when an event is
        about to be emitted.

        :param event: The event that will be sent
        :return: The observers to call.
        """
        pass


class ObservableImplementation(BaseObservableImplementation):
    def __init__(self):
        super().__init__()
        self._next_observer_id = 0
        self._observers = {}

    def add_event(self, event: str):
        self._observers[event] = {}

    def add_event_observer(self, event: str, observer: Callable) -> int:
        try:
            observers = self._observers[event]
        except KeyError:
            logger.warning('Trying to observe event "%s" not supported by %s',
                           event, self.__class__.__name__)
            observers = {}
            self._observers[event] = observers
        observer_id = self._next_observer_id
        self._next_observer_id += 1
        observers[observer_id] = observer
        return observer_id

    def remove_event_observer(self, observer_id: int):
        for observers in self._observers.values():
            try:
                observers.pop(observer_id)
            except KeyError:
                pass
            else:
                break

    def remove_all_observers(self):
        for events in self._observers.values():
            events.clear()
        self._next_observer_id = 0

    def get_event_observers(self, event: str) -> Iterable[Callable]:
        return self._observers.get(event, {}).values()


class ObservableInterface:
    """ This interface provides methods to register/deregister observers for a given event, emit events.
        It relies on an observable implementation which is accessed by get_observable_implementation method.

        Can be used safely in multiple inheritance.
    """

    EVENT_REMOVED = 'removed'
    EVENT_ON_CHANGED = 'changed'

    @abstractmethod
    def get_observable_implementation(self) -> BaseObservableImplementation:
        pass

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        """Return the names of events supported by the class."""
        return {ObservableInterface.EVENT_REMOVED, ObservableInterface.EVENT_ON_CHANGED}

    def init_events(self):
        """Call this method to initialize the BaseObservableImplementation"""
        for event in self.get_supported_events():
            self.get_observable_implementation().add_event(event)

    def add_event_observer(self, event: str, observer: Callable) -> int:
        """ Called to register an observer for event."""
        return self.get_observable_implementation().add_event_observer(event, observer)

    def remove_event_observer(self, observer_id: int):
        """ Called to remove an observer"""
        self.get_observable_implementation().remove_event_observer(observer_id)

    def do_remove(self):
        """
        Called when the observable is removed. Redefine this method to perform actions before the
        EVENT_REMOVED is emitted.
        :return:
        """
        return

    def remove(self):
        """ Called to remove the observable"""
        self.do_remove()
        self.emit(ObservableInterface.EVENT_REMOVED)
        self.get_observable_implementation().remove_all_observers()

    def emit(self, event: str, *args):
        """ Called to emit an event."""
        # Do not loop directly on the observers dictionary because callbacks that removes observer would lead to errors.
        observers = [o for o in self.get_observable_implementation().get_event_observers(event)]
        for observer in observers:
            observer(self, *args)


class Observable(ObservableInterface):
    """ Complete implementation of Observable.
        Can be used as mother class if no multiple inheritance is needed.
    """

    def __init__(self) -> None:
        super().__init__()
        self._obs_impl = ObservableImplementation()
        self.init_events()

    def get_observable_implementation(self) -> BaseObservableImplementation:
        return self._obs_impl
