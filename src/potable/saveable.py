# Copyright 2020, Matthieu Charbonnier

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from abc import abstractmethod
from collections import deque
from typing import Any, Dict, Iterator, Tuple, Iterable, Union, Type


class State:
    def __init__(self):
        # Version informations
        self._versions = {}
        # Parameters needed to reset the state of the Saveable
        self._parameters = {}

    def get_versions(self) -> Dict[str, str]:
        return self._versions

    def get_parameters(self) -> Dict[str, Any]:
        return self._parameters

    def add_version(self, key, version):
        self._versions[key] = version

    def add_parameter(self, key, value):
        self._parameters[key] = value

    def update_parameters(self, param_dict: Dict[str, Any]):
        self._parameters.update(param_dict)


PATH_SEP = '.'
BUILTINS = '__builtins__'
VERSION = '__version__'
CLASS = '__class__'
PATH = '__path__'


class Saveable:
    classes = {}

    def get_state(self) -> State:
        return State()

    def set_state(self, state: State):
        pass

    def get_object(self):
        return self

    @classmethod
    def class_path(cls):
        return 'root'

    @classmethod
    def class_name(cls) -> str:
        """ Override if a name different from the class name is desired.

        :return:
        """
        return cls.__name__

    @classmethod
    def register_class(cls):
        try:
            Saveable.classes[cls.class_path()][cls.class_name()] = cls
        except KeyError:
            Saveable.classes[cls.class_path()] = {cls.class_name(): cls}


class BuiltinSaver(Saveable):
    def __init__(self, builtin_object=None) -> None:
        super().__init__()
        if builtin_object is None:
            builtin_object = self.get_class()()
        self.object = builtin_object

    def get_object(self):
        return self.object

    @classmethod
    def class_path(cls):
        return BUILTINS

    @classmethod
    def class_name(cls) -> str:
        return cls.get_class().__name__

    @classmethod
    def get_class(cls) -> Type:
        pass


class IterableSaver(BuiltinSaver):
    def get_state(self) -> State:
        state = State()
        state.add_version(self.class_name(), '1')
        parameters = {'n': len(self.object)}
        for k, v in self.iter_save():
            parameters[k] = v
        state.update_parameters(parameters)
        return state

    @abstractmethod
    def iter_save(self) -> Iterable[Tuple[str, Any]]:
        pass

    def set_state(self, state: State):
        parameters = state.get_parameters()
        n_elements = parameters['n']
        self.state_load(n_elements, state)

    @abstractmethod
    def state_load(self, n_elements, state: State):
        pass


class DictSaver(IterableSaver):
    @classmethod
    def get_class(cls):
        return dict

    def iter_save(self) -> Iterable[Tuple[str, Any]]:
        for i, (k, v) in enumerate(self.object.items()):
            i_str = str(i)
            yield 'k' + i_str, k
            yield 'v' + i_str, v

    def state_load(self, n_elements, state: State):
        parameters = state.get_parameters()
        for index in range(n_elements):
            i_str = str(index)
            k = parameters['k' + i_str]
            v = parameters['v' + i_str]
            self.object[k] = v


DictSaver.register_class()


class ListSaver(IterableSaver):
    @classmethod
    def get_class(cls):
        return list

    def iter_save(self) -> Iterable[Tuple[str, Any]]:
        for i, v in enumerate(self.object):
            yield str(i), v

    def state_load(self, n_elements, state: State):
        parameters = state.get_parameters()
        for index in range(n_elements):
            self.object.append(parameters[str(index)])


ListSaver.register_class()


class SetSaver(ListSaver):
    @classmethod
    def get_class(cls) -> Type:
        return set

    def state_load(self, n_elements, state: State):
        parameters = state.get_parameters()
        for index in range(n_elements):
            self.object.add(parameters[str(index)])


SetSaver.register_class()


class TupleSaver(ListSaver):
    @classmethod
    def get_class(cls):
        return tuple

    def state_load(self, n_elements, state: State):
        parameters = state.get_parameters()
        self.object = tuple([parameters[str(i)] for i in range(n_elements)])


TupleSaver.register_class()


class SPendingState:
    def __init__(self, pending_state: Dict[str, Any], state_gen: Iterator[Dict[str, Any]], pending_key=None):
        self.state_gen = state_gen
        self.pending_key = None
        self.state = pending_state


class Serializer:
    def __init__(self):
        self.pending_queue = None

    def serialize(self, saveable) -> Dict[str, Any]:
        self.pending_queue = deque()
        self.initialize_saveable(saveable)
        cur_state = None
        out_state = None
        while self.pending_queue:
            pending_state = self.pending_queue.pop()
            ":type: SPendingState"
            out_state = pending_state.state
            if pending_state.pending_key:
                pending_state.state[pending_state.pending_key] = cur_state
                cur_state = None
            get_next_value = True
            while get_next_value:
                try:
                    k, v = next(pending_state.state_gen)
                except StopIteration:
                    cur_state = pending_state.state
                    get_next_value = False
                else:
                    get_next_value = self.serialize_state_value(k, v, pending_state)
        return out_state

    def serialize_state_value(self, k, v, pending_state: SPendingState) -> bool:
        if isinstance(v, Saveable):
            pending_state.pending_key = k
            self.pending_queue.append(pending_state)
            self.initialize_saveable(v)
            return False
        else:
            try:
                saveable = Saveable.classes[BUILTINS][v.__class__.__name__](v)
                pending_state.pending_key = k
                self.pending_queue.append(pending_state)
                self.initialize_saveable(saveable)
                return False
            except KeyError:
                pending_state.state[k] = v
                return True

    def initialize_saveable(self, saveable, pending_key=None):
        state = saveable.get_state()
        s_state = {PATH_SEP.join([VERSION, k]): v for k, v in state.get_versions().items()}
        s_state[CLASS] = saveable.class_name()
        s_state[PATH] = saveable.class_path()
        pending_state = SPendingState(s_state, iter(state.get_parameters().items()), pending_key=pending_key)
        self.pending_queue.append(pending_state)


class DPendingState:
    def __init__(self, state_gen: Iterator[Dict[str, Any]], pending_key=None):
        self.state_gen = state_gen
        self.pending_key = None
        self.state = State()
        self.saveable_path = ''
        self.saveable_class = ''

    def set_key_value(self, key, value):
        if isinstance(key, str):
            if key == PATH:
                self.saveable_path = value
            elif key == CLASS:
                self.saveable_class = value
            elif key.startswith(VERSION):
                v_name = key.split('.')[1]
                self.state.add_version(v_name, value)
            else:
                self.state.add_parameter(key, value)
        else:
            self.state.add_parameter(key, value)

    def build(self):
        saveable = Saveable.classes[self.saveable_path][self.saveable_class]()
        saveable.set_state(self.state)
        return saveable.get_object()


class Deserializer:
    def __init__(self):
        self.pending_queue = None

    def deserialize(self, serialized: dict) -> Union[Saveable, Dict]:
        self.pending_queue = deque()
        self.inititialize_serialized(serialized)

        cur_saveable = None
        while self.pending_queue:
            d_pending = self.pending_queue.pop()
            ":type: DPendingState"
            if d_pending.pending_key:
                d_pending.set_key_value(d_pending.pending_key, cur_saveable)
                cur_saveable = None
            while True:
                try:
                    k, v = next(d_pending.state_gen)
                except StopIteration:
                    cur_saveable = d_pending.build()
                    break
                else:
                    if isinstance(v, dict):
                        d_pending.pending_key = k
                        self.pending_queue.append(d_pending)
                        self.inititialize_serialized(v, k)
                        break
                    else:
                        d_pending.set_key_value(k, v)

        return cur_saveable


    def inititialize_serialized(self, serialized: dict, pending_key=None):
        pending_state = DPendingState(iter(serialized.items()), pending_key=pending_key)
        self.pending_queue.append(pending_state)
